# OpenML dataset: California-Housing-Classification

https://www.openml.org/d/45578

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Binarized version of the California Housing Dataset

This dataset was obtained from Luis Torgo's collection of regression datasets. It was binarized to serve as the original, unprocessed date for the California Housing dataset used by:

> Grinsztajn, Leo and Oyallon, Edouard and Varoquaux, Gael
> Why do tree-based models still outperform deep learning on typical tabular data?
> In: Advances in Neural Information Processing Systems (2022)

## Description:

This is a dataset obtained from the StatLib repository. Here is the included description:

S&P Letters Data
We collected information on the variables using all the block groups in California from the 1990 Cens us. In this sample a block group on average includes 1425.5 individuals living in a geographically co mpact area. Naturally, the geographical area included varies inversely with the population density. W e computed distances among the centroids of each block group as measured in latitude and longitude. W e excluded all the block groups reporting zero entries for the independent and dependent variables. T he final data contained 20,640 observations on 9 variables. The dependent variable is ln(median house value).

|                             | Bols     | tols     |
|-----------------------------|----------|----------|
| INTERCEPT                   | 11.4939  | 275.7518 |
| MEDIAN INCOME               | 0.4790   | 45.7768  |
| MEDIAN INCOME2              | -0.0166  | -9.4841  |
| MEDIAN INCOME3              | -0.0002  | -1.9157  |
| ln(MEDIAN AGE)              | 0.1570   | 33.6123  |
| ln(TOTAL ROOMS/ POPULATION) | -0.8582  | -56.1280 |
| ln(BEDROOMS/ POPULATION)    | 0.8043   | 38.0685  |
| ln(POPULATION/ HOUSEHOLDS)  | -0.4077  | -20.8762 |
| ln(HOUSEHOLDS)              | 0.0477   | 13.0792  |

The file contains all the the variables. Specifically, it contains median house value, med ian income, housing median age, total rooms, total bedrooms, population, households, latitude, and lo ngitude in that order.

Reference
Pace, R. Kelley and Ronald Barry, Sparse Spatial Autoregressions, Statistics and Probability Letters, 33 (1997) 291-297.

The manuscript describing the data can be found at www.spatial-statistics.com. The data are also available as Matlab files.

Contact kelley@spatial-statistics.com or kelley@pace.am if you have any further questions. Thanks.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45578) of an [OpenML dataset](https://www.openml.org/d/45578). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45578/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45578/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45578/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

